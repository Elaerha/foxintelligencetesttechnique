# Script realisé par Geoffroy DIMUR dans le cadre d'un test technique pour FoxIntelligence

import codecs
import json
import re
from locale import setlocale, LC_ALL
from datetime import datetime
from bs4 import BeautifulSoup


def main():
    setlocale(LC_ALL, 'fr_FR.utf-8')
    successful_read = False

    while not successful_read:
        try:
            print("Saisir le chemin vers le fichier html")
            f = codecs.open(input("Chemin : "), 'r', 'utf-8')

        except FileNotFoundError:
            print("Impossible d'ouvrir le fichier")
        else:
            successful_read = True
            soup = BeautifulSoup(f.read(), "html.parser")
            create_json(soup)


def get_date(soup):
    # On récupère la date de commande dans l'introduction avec du regex

    anchors = [td.find_all("td") for td in soup.find_all("table", id=r'\"intro\"')]
    match = re.search(r'\d{2}/\d{2}/\d{4}', anchors[0][4].text)
    date = datetime.strptime(match.group(), '%d/%m/%Y').date()
    return date


def get_passengers_on_train(ticket_number, soup):
    # On récupère le type de billet et la tranche d'age de tous les passagers pour un ticket donné

    json_file = {"passengers": []}
    anchors_passengers = [td.find_all("td") for td in soup.find_all(class_=r'\"passengers\"')]
    for n_passenger in range(0, int(len(anchors_passengers[ticket_number]) / 6)):
        age = re.search('\(([^)]+)', anchors_passengers[ticket_number][(n_passenger * 6) + 2].text).group(1)
        if "Billet échangeable" in anchors_passengers[ticket_number][(n_passenger * 6) + 3].text:
            echangeable = "échangeable"
        else:
            echangeable = "non-échangeable"
        json_file["passengers"].append({
            "type": echangeable,
            "age": age
        })
    return json_file["passengers"]


def get_trains(n_ticket, anchors_train, soup):
    # On récupère les inforamtions du ticket de train donné

    departure_time = anchors_train[n_ticket][1].text
    departure_station = anchors_train[n_ticket][2].text
    type_train = anchors_train[n_ticket][3].text
    train_number = anchors_train[n_ticket][4].text
    arrival_time = anchors_train[n_ticket][7].text
    arrival_station = anchors_train[n_ticket][8].text
    json_file = {'trains': []}
    json_file['trains'].append({
        "departureTime": departure_time,
        "departureStation": departure_station,
        "arrivalTime": arrival_time,
        "arrivalStation": arrival_station,
        "type": type_train,
        "number": train_number,
        "passengers": get_passengers_on_train(n_ticket, soup)
    })
    return json_file['trains']


def get_all_trips(soup):
    # On récupère la liste de tous élèments des blocks pnr qui contiennent les informations de dossier
    # Ici on ne traite que les billets de train et pas les cartes qui partagent la meme classe

    anchors_trips = [td.find_all("td") for td in soup.find_all(class_=r'\"block-pnr\"')]
    anchors_dates = soup.find_all(class_=r'\"product-travel-date\"')
    anchors_train = [td.find_all("td") for td in soup.find_all(class_=r'\"product-details\"')]
    n_ticket = 0
    json_file = {}
    for n_block_pnr in anchors_trips:  # Pour tous les blocks "block-pnr"
        if "<>" in n_block_pnr[0].text:  # il s'agit d'un aller-retour

            if 'roundTrips' not in json_file:
                json_file['roundTrips'] = []

            for i in range(2):

                # Calcul de l'année du billet
                date = datetime.strptime(anchors_dates[n_ticket].text, ' %A %d %B ')

                if date.month < get_date(soup).month or date.month == get_date(soup).month \
                        and date.day < get_date(soup).day:
                    date = date.replace(year=get_date(soup).year + 1)
                else:
                    date = date.replace(year=get_date(soup).year)

                trip_type = anchors_train[n_ticket][0].text
                train_codes = re.findall(r'[A-Z]{6}', n_block_pnr[1].text)
                json_file["roundTrips"].append({
                    "code": train_codes[i],
                    "type": trip_type,
                    "date": date.strftime('%Y-%m-%d'),
                    "trains": get_trains(n_ticket, anchors_train, soup)
                })
                n_ticket = n_ticket + 1

        elif '<' in n_block_pnr[0].text or '>' in n_block_pnr[0].text:  # Aller ou retour simple

            if 'onewayTrips' not in json_file:
                json_file['onewayTrips'] = []

            date = datetime.strptime(anchors_dates[n_ticket].text, ' %A %d %B ')

            # Calcul de l'année du billet
            if (date.month < get_date(soup).month or (
                    date.month == get_date(soup).month and date.day < get_date(soup).day)):
                date = date.replace(year=get_date(soup).year + 1)
            else:
                date = date.replace(year=get_date(soup).year)

            trip_type = anchors_train[n_ticket][0].text
            train_codes = re.findall(r'[A-Z]{6}', n_block_pnr[1].text)
            json_file['onewayTrips'].append({
                "code": train_codes[0],
                "type": trip_type,
                "date": date,
                "trains": get_trains(n_ticket, anchors_train, soup)
            })
            n_ticket = n_ticket + 1
    return json_file


def get_oneway_trips(json_file):
    return json_file['onewayTrips']


def get_round_trips(json_file):
    return json_file['roundTrips']


def get_trips(soup):
    # On recupere les champs du champs trips de la table

    json_file = {"price": re.findall(r'[-+]?\d*,\d+|\d+', soup.find(class_=r'\"very-important\"').text)[0]}
    trips = get_all_trips(soup)
    if 'onewayTrips' in trips:
        json_file['onewayTrips'] = []
        json_file['onewayTrips'].append(get_oneway_trips(trips))
    if 'roundTrips' in trips:
        json_file['roundTrips'] = []
        json_file['roundTrips'].append(get_round_trips(trips))
    return json_file


def get_cards(soup):
    # On recupere les informations de carte de la meme maniere que pour les informations de dossier

    anchors_trips = [td.find_all("td") for td in soup.find_all(class_=r'\"block-pnr\"')]
    json_file = {'cards': []}
    for n_block_pnr in anchors_trips:  # Pour tous les blocks "block-pnr"
        if len(n_block_pnr) == 2:
            code = re.search(r'[A-Z]{6}', n_block_pnr[0].text).group()
            name = re.search(r'[a-z]+', n_block_pnr[1].text.replace(r' Nom associé', '').replace(r':  ', '')).group()
            json_file['cards'].append({
                'code': code,
                'name': name
            })
    return json_file['cards']


def get_custom(soup):
    # On recupere les differents prix à payer

    json_file = {'prices': []}
    anchors_prices = [td.find_all("td") for td in soup.find_all("table", class_=r'\"product-header\"')]
    for tickets in anchors_prices:
        json_file['prices'].append({
            'value': re.search(r'[0-9]+,[0-9]+', tickets[6].text).group()
        })
    for cards in soup.find_all("td", class_=r'\"amount\"'):
        json_file['prices'].append({
            'value': re.search(r'[0-9]+,[0-9]+', cards.text).group()
        })
    return json_file


def get_result(soup):
    # On recupere tous les champs du fichier resultat json

    json_file = {
        'trips': get_trips(soup),
        'cards': get_cards(soup),
        'custom': get_custom(soup)
    }
    return json_file


def create_json(soup):
    # On crée le fichier json avec tous nos champs
    json_file = {
        'status': 'ok',
        'result': get_result(soup)
    }
    try:
        with open('data.json', 'w') as outfile:
            json.dump(json_file, outfile)
            outfile.close()
    except IOError:
        print("Erreur lors de la production du fichier JSON")

    else:
        print("Le fichier a été produit sous le nom 'data.json' à la source du programme")


main()
