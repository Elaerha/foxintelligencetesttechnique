Bonjour,

Afin de réaliser ce script, j'ai dû considérer que les conditions suivantes étaient vérifiées :

- La structure générale (noms de classe) du fichier reste la même
- Le billet est en français

j'ai aussi pris quelques libertés quant au rendu du fichier JSON :

- J'ai trouvé plus judicieux d'associé à chaque billet de train son numéro de dossier et à chaque carte son numéro de dossier pour pouvoir retrouver les references de chacun des items indépendamment.

- J'ai associé les voyageurs à chaque billet de train de manière individuelle, ainsi pour chaque billet nous pouvoir voir la tranche d'age du passager et le type (échangeable ou non)

Afin de traiter les données du fichier HTML, j'ai utilisé les librairies suivantes : 

BeautifulSoup4

	pip install bs4

Codecs
	pip install codecs

Datetime
	pip install datetime

Le fichier a été codé et compilé sous Mac OS, l'executable est donc compatible Mac/UNIX et situé dans le dossier dist.
Le code source est situé dans le fichier main.py.

Merci.

Cordialement,

Geoffroy DIMUR